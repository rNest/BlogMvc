﻿using BlogMvc.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogMvc.Models.AppModels
{
    public class AppPerson : BaseEntity
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string PersonType { get; set; }
        public Guid RawGuid { get; set; }
    }
}