﻿using BlogMvc.Library.HtmlHelpers.Table;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.WebPages;

namespace BlogMvc.Library.HtmlHelpers
{
    public static class InlineHelper
    {
        
        public static InlineMethods UI(this HtmlHelper helper)
        {
            return new InlineMethods(helper);
        }

    }

    public class InlineMethods : IDisposable
    {
        protected HtmlHelper _helper = null;
        public InlineMethods(HtmlHelper helper)
        {
            _helper = helper;
        }

        public TagEnd BeginRowBox(string cssClass = "row", string id = "", string style = "")
        {
            TagBuilder rowBuilder = new TagBuilder("div");
            rowBuilder.MergeAttribute("class", cssClass);
            if (!string.IsNullOrEmpty(style))
            {
                rowBuilder.MergeAttribute("style", style);
            }
            if (!string.IsNullOrEmpty(id))
            {
                rowBuilder.MergeAttribute("id", id);
            }
            _helper.ViewContext.Writer.Write(rowBuilder.ToString(TagRenderMode.StartTag));
            return new TagEnd(_helper.ViewContext, "div");
        }

        public TagEnd BeginColumn(int cols = 12, int offset = 0, string additionalClass = "", string id = "", string style = "")
        {
            TagBuilder columnBuilder = new TagBuilder("div");
            string colDef = string.Format("col-xs-12 col-sm-{0} col-md-{0} col-lg-{0}", cols);
            columnBuilder.MergeAttribute("class", colDef);
            if (offset > 0)
            {
                colDef = string.Format("col-sm-offset-{0} col-md-offset-{0} col-lg-offset-{0}", offset);
                columnBuilder.AddCssClass(colDef);
            }
            if (!string.IsNullOrEmpty(additionalClass))
            {
                columnBuilder.AddCssClass(additionalClass);
            }
            
            if(!string.IsNullOrEmpty(id))
            {
                columnBuilder.MergeAttribute("id", id);
            }
            
            if (!string.IsNullOrEmpty(style))
            {
                columnBuilder.MergeAttribute("style", style);
            }
            
            _helper.ViewContext.Writer.Write(columnBuilder.ToString(TagRenderMode.StartTag));
            return new TagEnd(_helper.ViewContext, "div");
        }

        public TableControl<T> Table<T>(IEnumerable<T> list) where T : class
        {
            return new TableControl<T>(list, _helper);
        }

        public TagEnd Dropdown()
        {
            return new TagEnd(_helper.ViewContext, "");
        }
        public MvcHtmlString SpanText(string text, string cssClass = null, string id = null)
        {
            TagBuilder span = new TagBuilder("span");
            if (null != id)
            {
                span.MergeAttribute("id", id);
            }
            if (!string.IsNullOrEmpty(cssClass))
            {
                span.AddCssClass(cssClass);
            }
            span.SetInnerText(text);
            string html = string.Format("{0}", span.ToString(TagRenderMode.Normal));
            return new MvcHtmlString(html);
        }

        public MvcHtmlString Paragraph(string text, string cssClass = null, string id = null)
        {
            TagBuilder p = new TagBuilder("p");
            if (null != id)
            {
                p.MergeAttribute("id", id);
            }
            if (!string.IsNullOrEmpty(cssClass))
            {
                p.AddCssClass(cssClass);
            }
            p.SetInnerText(text);
            return new MvcHtmlString(p.ToString(TagRenderMode.Normal));
        }

        public TagEnd BeginLink(string href, string id = null, string cssClass = null, bool newTab = false, string style = null)
        {
            TagBuilder linkBuilder = new TagBuilder("a");
            linkBuilder.MergeAttribute("href", href);
            if (!string.IsNullOrEmpty(id))
            {
                linkBuilder.MergeAttribute("id", id);
            }
            if (!string.IsNullOrEmpty(cssClass))
            {
                linkBuilder.MergeAttribute("class", cssClass);
            }
            if (!string.IsNullOrEmpty(style))
            {
                linkBuilder.MergeAttribute("style", style);
            }
            if(newTab && !string.IsNullOrEmpty(href))
            {
                linkBuilder.MergeAttribute("target", "_blank");
            }
            _helper.ViewContext.Writer.Write(linkBuilder.ToString(TagRenderMode.StartTag));
            return new TagEnd(_helper.ViewContext, "a");
        }

        public MvcHtmlString Link(string href, string text, string id = null, string cssClass = null, bool newTab = false, string style = null)
        {
            TagBuilder linkBuilder = new TagBuilder("a");
            linkBuilder.MergeAttribute("href", href);
            if (!string.IsNullOrEmpty(id))
            {
                linkBuilder.MergeAttribute("id", id);
            }
            if (!string.IsNullOrEmpty(cssClass))
            {
                linkBuilder.MergeAttribute("class", cssClass);
            }
            if (!string.IsNullOrEmpty(style))
            {
                linkBuilder.MergeAttribute("style", style);
            }
            if (newTab && !string.IsNullOrEmpty(href))
            {
                linkBuilder.MergeAttribute("target", "_blank");
            }
            linkBuilder.SetInnerText(text);
            return new MvcHtmlString(linkBuilder.ToString(TagRenderMode.Normal));
        }

        public TagEnd BeginBox(string id = null, string cssClass = null, string style = null)
        {
            TagBuilder divBuilder = new TagBuilder("div");
            if (!string.IsNullOrEmpty(id))
            {
                divBuilder.MergeAttribute("id", id);
            }
            if (!string.IsNullOrEmpty(cssClass))
            {
                divBuilder.MergeAttribute("class", cssClass);
            }
            if (!string.IsNullOrEmpty(style))
            {
                divBuilder.MergeAttribute("style", style);
            }
            _helper.ViewContext.Writer.Write(divBuilder.ToString(TagRenderMode.StartTag));
            return new TagEnd(_helper.ViewContext, "div");
        }

        public TagEnd BeginContainer(string id = null, string cssClass = null, string style = null)
        {
            TagBuilder containerBuilder = new TagBuilder("div");
            containerBuilder.MergeAttribute("class", "container");
            if (!string.IsNullOrEmpty(id))
            {
                containerBuilder.MergeAttribute("id", id);
            }
            if (!string.IsNullOrEmpty(cssClass))
            {
                containerBuilder.AddCssClass(cssClass);
            }
            if (!string.IsNullOrEmpty(style))
            {
                containerBuilder.MergeAttribute("style", style);
            }
            _helper.ViewContext.Writer.Write(containerBuilder.ToString(TagRenderMode.StartTag));
            return new TagEnd(_helper.ViewContext, "div");
        }

        public TagEnd BeginSmall(string id = null, string cssClass = null, string style = null)
        {
            TagBuilder smallBuilder = new TagBuilder("small");
            if (!string.IsNullOrEmpty(id))
            {
                smallBuilder.MergeAttribute("id", id);
            }
            if (!string.IsNullOrEmpty(cssClass))
            {
                smallBuilder.MergeAttribute("class", cssClass);
            }
            if (!string.IsNullOrEmpty(style))
            {
                smallBuilder.MergeAttribute("style", style);
            }
            _helper.ViewContext.Writer.Write(smallBuilder.ToString(TagRenderMode.StartTag));
            return new TagEnd(_helper.ViewContext, "small");
        }

        public MvcHtmlString Small(string id = null, string cssClass = null, string style = null)
        {
            TagBuilder smallBuilder = new TagBuilder("small");
            if (!string.IsNullOrEmpty(id))
            {
                smallBuilder.MergeAttribute("id", id);
            }
            if (!string.IsNullOrEmpty(cssClass))
            {
                smallBuilder.MergeAttribute("class", cssClass);
            }
            if (!string.IsNullOrEmpty(style))
            {
                smallBuilder.MergeAttribute("style", style);
            }
            
            return new MvcHtmlString(smallBuilder.ToString(TagRenderMode.Normal));
        }
        public MvcHtmlString Strong(string id = null, string cssClass = null, string style = null)
        {
            TagBuilder strongBuilder = new TagBuilder("strong");
            if (!string.IsNullOrEmpty(id))
            {
                strongBuilder.MergeAttribute("id", id);
            }
            if (!string.IsNullOrEmpty(cssClass))
            {
                strongBuilder.MergeAttribute("class", cssClass);
            }
            if (!string.IsNullOrEmpty(style))
            {
                strongBuilder.MergeAttribute("style", style);
            }

            return new MvcHtmlString(strongBuilder.ToString(TagRenderMode.Normal));
        }
        public TagEnd BeginStrong(string id = null, string cssClass = null, string style = null)
        {
            TagBuilder strongBuilder = new TagBuilder("strong");
            if (!string.IsNullOrEmpty(id))
            {
                strongBuilder.MergeAttribute("id", id);
            }
            if (!string.IsNullOrEmpty(cssClass))
            {
                strongBuilder.MergeAttribute("class", cssClass);
            }
            if (!string.IsNullOrEmpty(style))
            {
                strongBuilder.MergeAttribute("style", style);
            }
            _helper.ViewContext.Writer.Write(strongBuilder.ToString(TagRenderMode.StartTag));
            return new TagEnd(_helper.ViewContext, "strong");
        }

        public void Dispose()
        {

        }

    }

    public class TableControl<T> where T : class
    {
        protected IEnumerable<T> _list = null;
        protected HtmlHelper _helper = null;
        protected IDictionary<string, string> _columns = null;
        protected IList<Func<T, object>> _columnLambdas = null;
        public TableControl(IEnumerable<T> list, HtmlHelper helper)
        {
            _list = list;
            _helper = helper;
            _columns = new Dictionary<string, string>();
            _columnLambdas = new List<Func<T, object>>();
        }

        public TableControl<T> Column(Expression<Func<T, object>> column, string displayName = null)
        {
            _columns.Add(string.IsNullOrEmpty(displayName) ? GetMemberName(column.Body) : displayName, GetMemberName(column.Body));
            _columnLambdas.Add(column.Compile());
            var x = _columns;
            return this;
        }

        protected string GetMemberName(Expression expression)
        {
            if (expression == null)
            {
                return "";
            }

            if (expression is MemberExpression)
            {
                // Reference type property or field
                var memberExpression = (MemberExpression)expression;
                return memberExpression.Member.Name;
            }

            if (expression is MethodCallExpression)
            {
                // Reference type method
                var methodCallExpression = (MethodCallExpression)expression;
                return methodCallExpression.Method.Name;
            }

            if (expression is UnaryExpression)
            {
                // Property, field of method returning value type
                var unaryExpression = (UnaryExpression)expression;
                return GetMemberName(unaryExpression.Operand);
            }

            return "";
        }


        public override string ToString()
        {
            TableModel model = new TableModel();
            model.TableBody = new List<dynamic>();
            model.TableHeader = _columns;
            ((List<dynamic>)model.TableBody).AddRange(_list);
            string content = _helper.Partial("~/Views/Shared/Table/_tableView.cshtml", model).ToString();
            _helper.ViewContext.Writer.Write(content);
            return "";
        }

    }

    public class TagEnd : IDisposable
    {
        protected TextWriter _writer = null;
        protected string _tag = "";
        public TagEnd(ViewContext ctx, string tag)
        {
            _writer = ctx.Writer;
            _tag = tag;
        }
        public void Dispose()
        {
            _writer.Write("</" + _tag + ">");
        }
    }

}