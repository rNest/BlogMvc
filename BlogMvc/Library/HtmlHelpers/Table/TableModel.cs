﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace BlogMvc.Library.HtmlHelpers.Table
{
    public class TableModel 
    {
        public IDictionary<string, string> TableHeader { get; set; }
        public IList<dynamic> TableBody { get; set; }
    }
}