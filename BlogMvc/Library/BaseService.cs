﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;

namespace BlogMvc.Library
{
    public abstract class BaseService<TEntity> : IDisposable where TEntity : BaseEntity
    {
        protected DbContext _context = null;

        public BaseService(DbContext context)
        {
            _context = context;
        }

        public int Add(TEntity entity)
        {
            SetCreatorId(entity);
            _context.Set<TEntity>().Add(entity);
            _context.SaveChanges();
            return -1;
        }

        public async Task<int> AddAsync(TEntity entity)
        {
            var user = HttpContext.Current.User;
            Task<int> task = Task.Factory.StartNew(() => 
            {
                SetCreatorId(entity, user);
                _context.Set<TEntity>().Add(entity);
                _context.SaveChanges();
                return -1;
            });

            try
            {
                return await task;
            }
            catch (AggregateException ae)
            {
                throw;
                return -999;
            }
            catch (Exception e)
            {
                throw;
                return -999;
            }
            
            
        }

        public bool Update(TEntity entity)
        {
            TEntity original = _context.Set<TEntity>().Find(entity.ID);
            if(null != original)
            {
                entity.CreatedBy = original.CreatedBy;
                entity.CreatedOn = original.CreatedOn;
                SetModificatorId(original);
                _context.Entry(original).CurrentValues.SetValues(entity);
                _context.SaveChanges();
                return true;
            }
            
            return false;
        }

        public async Task<bool> UpdateAsync(TEntity entity)
        {
            var user = HttpContext.Current.User;
            Task<bool> task = Task.Factory.StartNew(() => 
            {
                TEntity original = _context.Set<TEntity>().Find(entity.ID);
                if (null != original)
                {
                    entity.CreatedBy = original.CreatedBy;
                    entity.CreatedOn = original.CreatedOn;
                    SetModificatorId(entity, user);
                    _context.Entry(original).CurrentValues.SetValues(entity);
                    _context.SaveChanges();
                    return true;
                }

                return false;
            });
            try
            {
                return await task;
            }
            catch (AggregateException ae)
            {
                throw;
                return false;
            }
            catch (Exception e)
            {
                throw;
                return false;
            }
            
        }

        public TEntity Remove(TEntity entity)
        {
            _context.Set<TEntity>().Attach(entity);
            TEntity removed = _context.Set<TEntity>().Remove(entity);
            _context.SaveChanges();
            return removed;
        }

        public async Task<TEntity> RemoveAsync(TEntity entity)
        {
            Task<TEntity> task = Task.Factory.StartNew(() => 
            {
                _context.Set<TEntity>().Attach(entity);
                TEntity removed = _context.Set<TEntity>().Remove(entity);
                _context.SaveChanges();
                return removed;
            });
            try
            {
                return await task;
            }
            catch(Exception e)
            {
                throw;
                return null;
            }
            
        }

        public TEntity Find(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public async Task<TEntity> FindAsync(int id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public TEntity Find(Guid guid)
        {
            return _context.Set<TEntity>().Find(guid);
        }

        public async Task<TEntity> FindAsync(Guid guid)
        {
            return await _context.Set<TEntity>().FindAsync(guid);
        }

        public IList<TEntity> GetAll()
        {
            return _context.Set<TEntity>().ToList();
        }

        public async Task<IList<TEntity>> GetAllAsync()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        protected void SetCreatorId(TEntity entity)
        {
            string guid = HttpContext.Current.User?.Identity?.GetUserId();
            if (!string.IsNullOrEmpty(guid))
            {
                entity.CreatedBy = guid;
                entity.CreatedOn = DateTime.Now;
            }

        }

        protected void SetCreatorId(TEntity entity, IPrincipal user)
        {
            string guid = user?.Identity?.GetUserId();
            if (!string.IsNullOrEmpty(guid))
            {
                entity.CreatedBy = guid;
                entity.CreatedOn = DateTime.Now;
            }

        }

        protected void SetModificatorId(TEntity entity)
        {
            string guid = HttpContext.Current.User?.Identity?.GetUserId();
            if (!string.IsNullOrEmpty(guid))
            {
                entity.ModifiedBy = guid;
                entity.ModifiedOn = DateTime.Now;
            }
            
        }

        protected void SetModificatorId(TEntity entity, IPrincipal user)
        {
            string guid = user?.Identity?.GetUserId();
            if (!string.IsNullOrEmpty(guid))
            {
                entity.ModifiedBy = guid;
                entity.ModifiedOn = DateTime.Now;
            }

        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}