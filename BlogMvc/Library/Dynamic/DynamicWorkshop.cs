﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Web;

namespace BlogMvc.Library.Dynamic
{
    public class DynamicWorkshop
    {

        public static PropertyInfo GetDynamicPropertyByName(dynamic source, string propertyName)
        {
            object sourceObject = source;
            return sourceObject.GetType().GetProperty(propertyName);
        }

        public static PropertyInfo GetObjectPropertyByName(object source, string propertyName)
        {
            return source.GetType().GetProperty(propertyName);
        }

        private static void AddExpandoProperties(ExpandoObject expando, params string[] columns)
        {
            IDictionary<string, object> expandoDictionary = expando as IDictionary<string, object>;
            if (null != expandoDictionary && columns.Length > 0)
            {
                foreach (string attribute in columns)
                {
                    if (!expandoDictionary.ContainsKey(attribute))
                    {
                        expandoDictionary.Add(attribute, null);
                    }
                }

            }
        }

        public static void EmitNewObject(string newTypeName, bool alive = false, Type basetype = null, params string[] attributes)
        {
            AppDomain localDomain = AppDomain.CurrentDomain;
            AssemblyName name = new AssemblyName("dynamicObjects_" + Guid.NewGuid().ToString());
            AssemblyBuilder assemblyBuilder = localDomain.DefineDynamicAssembly(name, AssemblyBuilderAccess.Run);
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("dynamicModule_" + Guid.NewGuid().ToString());
            TypeBuilder typeBuilder = moduleBuilder.DefineType(newTypeName, TypeAttributes.Public);
            if(null != basetype)
            {
                typeBuilder.SetParent(basetype);
            }

            if(attributes.Length > 0)
            {
                foreach (string attribute in attributes)
                {
                    PropertyBuilder pb  = typeBuilder.DefineProperty(attribute, PropertyAttributes.HasDefault, typeof(object), null);
                    //ILGenerator generator = pb.
                }
            }

            

            if(alive)
            {

            }

        }
    }
}