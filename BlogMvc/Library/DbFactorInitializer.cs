﻿using BlogMvc.Models.AppModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BlogMvc.Library
{
    public class DbFactorInitializer : DropCreateDatabaseIfModelChanges<DbContext>
    {
        public override void InitializeDatabase(DbContext context)
        {
            base.InitializeDatabase(context);
        }

        protected override void Seed(DbContext context)
        {
            
            base.Seed(context);
        }

    }
}