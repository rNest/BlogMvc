﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BlogMvc
{
    public class MvcApplication : System.Web.HttpApplication
    {

        public MvcApplication()
        {
            //RequestHandler();
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_End()
        {
            
        }

        void RequestHandler()
        {
            this.BeginRequest += Lambda;
            this.AuthenticateRequest += Lambda;
            this.AuthorizeRequest += Lambda;
            this.ResolveRequestCache += Lambda;
            this.MapRequestHandler += Lambda;
            this.AcquireRequestState += Lambda;
            this.PreRequestHandlerExecute += Lambda;
            this.ReleaseRequestState += Lambda;
            this.UpdateRequestCache += Lambda;
            this.LogRequest += Lambda;
            this.PostLogRequest += Lambda;
            this.EndRequest += Lambda;
            this.PreSendRequestContent += Lambda;
            this.PreSendRequestHeaders += Lambda;
            this.PostReleaseRequestState += Lambda;
            this.PostAuthenticateRequest += Lambda;
            this.PostResolveRequestCache += Lambda;
            this.PostMapRequestHandler += Lambda;
            this.PostAcquireRequestState += Lambda;
            this.PostRequestHandlerExecute += Lambda;
        }

        void Lambda(object sender, EventArgs args)
        {
            HttpApplication app = sender as HttpApplication;
            if (null != app)
            {
                string eventName = app.Context.CurrentNotification.ToString();
                if (app.Context.IsPostNotification)
                    eventName = "Post" + eventName;
                if (eventName == RequestNotification.BeginRequest.ToString())
                {
                    
                }
                
            }
        }

        protected void Application_BeginRequest()
        {
            Lambda(this, new EventArgs());//1
        }
        protected void Application_AuthenticateRequest()
        {
            Lambda(this, new EventArgs());//2
        }
        protected void Application_PostAuthenticateRequest()
        {
            Lambda(this, new EventArgs());//3
        }
        protected void Application_AuthorizeRequest()
        {
            Lambda(this, new EventArgs());//4
        }
        protected void Application_ResolveRequestCache()
        {
            Lambda(this, new EventArgs());//5
        }
        protected void Application_PostResolveRequestCache()
        {
            Lambda(this, new EventArgs());//6
        }
        protected void Application_MapRequestHandler()
        {
            Lambda(this, new EventArgs());//7
        }
        protected void Application_PostMapRequestHandler()
        {
            Lambda(this, new EventArgs());//8
        }
        protected void Application_AcquireRequestState()
        {
            Lambda(this, new EventArgs());//9
        }
        protected void Application_PostAcquireRequestState()
        {
            Lambda(this, new EventArgs());//10
        }
        protected void Application_PreRequestHandlerExecute()
        {
            Lambda(this, new EventArgs());//11
        }
        protected void Application_PostRequestHandlerExecute()
        {
            Lambda(this, new EventArgs());//12
        }
        protected void Application_ReleaseRequestState()
        {
            Lambda(this, new EventArgs());//13
        }
        protected void Application_PostReleaseRequestState()
        {
            Lambda(this, new EventArgs());//14
        }

        protected void Application_UpdateRequestCache()
        {
            Lambda(this, new EventArgs());//15
        }
        protected void Application_LogRequest()
        {
            Lambda(this, new EventArgs());//16
        }
        protected void Application_PostLogRequest()
        {
            Lambda(this, new EventArgs());//17
        }
        protected void Application_EndRequest()
        {
            Lambda(this, new EventArgs());//18
        }
        protected void Application_PreSendRequestContent()
        {
            Lambda(this, new EventArgs());//19
        }
        protected void Application_PreSendRequestHeaders()
        {
            Lambda(this, new EventArgs());//20
        }

    }
}
