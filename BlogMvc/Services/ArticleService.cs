﻿using BlogMvc.Library;
using BlogMvc.Models.AppModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BlogMvc.Services
{
    public class ArticleService : BaseService<Article>
    {
        public ArticleService(DbContext context) : base(context)
        {
        }
    }
}