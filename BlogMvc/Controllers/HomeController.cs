﻿using BlogMvc.Library;
using BlogMvc.Models.Adventure;
using BlogMvc.Models.AppModels;
using BlogMvc.Services;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogMvc.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
        }
        public ActionResult Index()
        {
            //_articleService.Add(new Article
            //{
            //    Content = "Test content",
            //    CreatedBy = User.Identity.GetUserId(),
            //    ModifiedBy = null,
            //    CreatedOn = DateTime.Now,
            //    ModifiedOn = null,
            //    SubTitle = "Pierwszy artykuł",
            //    Title = "Test tytuł"
            //});
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult People()
        {
            List<AppPerson> local = new List<AppPerson>();
            using (Adventure ctx = new Adventure())
            {
                var query = (from p in ctx.Person
                            select p).Take(100);
                foreach (Person item in query)
                {
                    local.Add(new AppPerson
                    {
                        FirstName = item.FirstName,
                        LastName = item.LastName,
                        ID = item.BusinessEntityID,
                        PersonType = item.PersonType,
                        Phone = 
                        (item.PersonPhone != null && item.PersonPhone.Count > 0) ? item.PersonPhone.FirstOrDefault().PhoneNumber : null,
                        RawGuid = item.rowguid
                    });
                }
            }

            return View(local);
        }

    }
}