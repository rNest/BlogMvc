﻿using BlogMvc.Library;
using Microsoft.Owin;
using Owin;
using System.Data.Entity;

[assembly: OwinStartupAttribute(typeof(BlogMvc.Startup))]
namespace BlogMvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureDB(app);
        }

        private void ConfigureDB(IAppBuilder app)
        {
            //using (DbFactor _factor = new DbFactor())
            //{
            //    _factor.Database.CreateIfNotExists();
                
            //}

            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DbFactor>());
        }

    }
}
