﻿using System.Web.Mvc;

namespace BlogMvc.Areas.Articles
{
    public class ArticlesAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Articles";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                name: "DefaultArticles",
                url: "",
                defaults: new { controller = "Article", action = "Index" }
                //namespaces: new string[] { "BlogMvc.Areas.Articles.Controllers.ArticleController" }
            );
            context.MapRoute(
                name: "ArticlesList",
                url: "artykul-lista",
                defaults: new { controller = "Article", action = "Index" }
            );
            context.MapRoute(
                name: "ArticlesCreate",
                url: "artykul-nowy",
                defaults: new { controller = "Article", action = "Create" }
            );
            context.MapRoute(
                name: "ArticlesEdit",
                url: "artykul-edycja",
                defaults: new { controller = "Article", action = "Edit" }
            );
            context.MapRoute(
                name: "ArticlesDetails",
                url: "artykul-szczegoly",
                defaults: new { controller = "Article", action = "Details" }
            );
            context.MapRoute(
                name: "ArticlesDelete",
                url: "artykul-usun",
                defaults: new { controller = "Article", action = "Delete" }
            );
            context.MapRoute(
                name: "ArticlesTest",
                url: "test",
                defaults: new { controller = "Article", action = "Test" }
            );

        }
    }
}